<?php

namespace Monkkey\CommonToolsBundle;

use Monkkey\CommonToolsBundle\DependencyInjection\MonkkeyCommonToolsExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MonkkeyCommonToolsBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new MonkkeyCommonToolsExtension();
        }
        return $this->extension;
    }
}
