# Common Tools Bundle

## Installation

```shell
composer require monkkey/common-tools-bundle
```

## Usage

Check the library repository : https://gitlab.com/monkkey/common-tools
